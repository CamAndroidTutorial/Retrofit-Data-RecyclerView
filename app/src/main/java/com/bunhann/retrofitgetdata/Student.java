package com.bunhann.retrofitgetdata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student implements Parcelable{

    @SerializedName("id")
    @Expose
    private Integer studentId;

    @SerializedName("student_name")
    @Expose
    private String studentName;

    @SerializedName("student_gender")
    @Expose
    private String studentGender;


    @SerializedName("age")
    @Expose
    private Integer studentAge;

    private String placeOfBirth;

    public Student() {
    }

    protected Student(Parcel in) {
        if (in.readByte() == 0) {
            studentId = null;
        } else {
            studentId = in.readInt();
        }
        studentName = in.readString();
        studentGender = in.readString();
        if (in.readByte() == 0) {
            studentAge = null;
        } else {
            studentAge = in.readInt();
        }
        placeOfBirth = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentGender() {
        return studentGender;
    }

    public void setStudentGender(String studentGender) {
        this.studentGender = studentGender;
    }

    public Integer getStudentAge() {
        return studentAge;
    }

    public void setStudentAge(Integer studentAge) {
        this.studentAge = studentAge;
    }


    @Override
    public String toString() {
        return this.getStudentName() + " : " + this.getStudentAge().toString() ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (studentId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(studentId);
        }
        dest.writeString(studentName);
        dest.writeString(studentGender);
        if (studentAge == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(studentAge);
        }
        dest.writeString(placeOfBirth);
    }
}
