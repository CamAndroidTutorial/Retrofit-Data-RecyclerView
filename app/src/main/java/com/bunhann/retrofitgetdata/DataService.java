package com.bunhann.retrofitgetdata;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DataService {

    @GET("student/")
    Call<ArrayList<Student>> getAllStudents();


    @GET("student/{id}")
    Call<Student> getStudent(@Path("id") int studentId);

    @POST("student/")
    Call<Student> createUser(@Body Student student);

    @PUT("student/{id}")
    Call<Student> updateStudent(@Path("id") int studentId, @Body Student student);

    @DELETE("student/{id}")
    Call<Student> deleteStudent(@Path("id") int studentId);
}
