package com.bunhann.retrofitgetdata;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerStudent;
    private ArrayList<Student> studentList;

    private StudentAdapter adapter;

    private DataService client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerStudent = (RecyclerView) findViewById(R.id.recyclerStudent);

        studentList = new ArrayList<>();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.menuAdd:

                Intent intent = new Intent(this, AddStudentActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        client = RestClient.getClient().create(DataService.class);

        client.getAllStudents().enqueue(new Callback<ArrayList<Student>>() {
            @Override
            public void onResponse(Call<ArrayList<Student>> call, Response<ArrayList<Student>> response) {
                if (response.isSuccessful()){
                    studentList = response.body();

                    adapter = new StudentAdapter(getBaseContext(), studentList);
                    recyclerStudent.setAdapter(adapter);
                    recyclerStudent.setLayoutManager(linearLayoutManager);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Student>> call, Throwable t) {

            }
        });

        recyclerStudent.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerStudent, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Student s = studentList.get(position);
                Toast.makeText(MainActivity.this, s.getStudentId().toString(), Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(view.getContext(), AddStudentActivity.class);
                intent.putExtra("STUDENT", s);
                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        super.onResume();
    }
}
